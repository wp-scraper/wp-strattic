<?php
/**
 * Plugin Name: Strattic
 * Plugin URI: https://strattic.com/
 * Description: Handle deployment and manage your Strattic site
 * Version: 1.0
 * Author: Strattic
 * Author URI: https://strattic.com
 * Requires at least: 4.4
 * Tested up to: 4.7
 *
 * Text Domain: strattic
 * Domain Path: /i18n/languages/
 *
 * @package Strattic
 * @category Core
 * @author Strattic
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}


require __DIR__ . '/inc/class-strattic-plugin.php';
$GLOBALS['strattic'] = new Strattic_Plugin;