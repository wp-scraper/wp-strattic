<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

class Strattic_Plugin {
	/**
	 * URLs and Paths used by the plugin
	 *
	 * @var array
	 */
	public $locations = array();

	/**
	 * @var Strattic_Admin
	 */
	public $admin;

	/**
	 * Class constructor
	 */
	public function __construct() {
		$locate = $this->requirements();
		$locate = $this->locate_plugin();

		$this->locations = array(
			'plugin'    => $locate['plugin_basename'],
			'dir'       => $locate['dir_path'],
			'url'       => $locate['dir_url'],
			'inc_dir'   => $locate['dir_path'] . 'includes/',
			'class_dir' => $locate['dir_path'] . 'classes/',
		);

		$this->admin = new Strattic_Admin( $this );
	}

	private function requirements() {
		require __DIR__ . '/class-strattic-admin.php';
		require __DIR__ . '/class-strattic-settings-fields.php';
	}

	/**
	 * Version of plugin_dir_url() which works for plugins installed in the plugins directory,
	 * and for plugins bundled with themes.
	 *
	 * @throws \Exception
	 *
	 * @return array
	 */
	private function locate_plugin() {
		$dir_url          = trailingslashit( plugins_url( '', dirname( __FILE__ ) ) );
		$dir_path         = plugin_dir_path( dirname( __FILE__ ) );
		$dir_basename     = basename( $dir_path );
		$plugin_basename  = trailingslashit( $dir_basename ) . $dir_basename . '.php';

		return compact( 'dir_url', 'dir_path', 'dir_basename', 'plugin_basename' );
	}
}