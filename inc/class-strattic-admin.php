<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}


/**
 * Deployment tool
 *
 * @author  Strattic <maor@strattic.com>
 */
class Strattic_Admin {
	protected $_screens = array();
	protected $options;
	public $slug = 'strattic-settings';

	public function __construct( $plugin ) {
		$this->plugin = $plugin;

		add_action( 'wp_ajax_strattic-do-deploy', array($this, 'do_request') );
		add_action( 'wp_footer', array($this, 'footer_print_js') );
		add_action( 'admin_footer', array($this, 'footer_print_js') );
		add_action( 'wp_before_admin_bar_render', array($this, 'custom_toolbar'), 999 );

		add_action( 'admin_menu', array( $this, 'create_admin_menu' ), 20 );
		add_action( 'admin_init', array( $this, 'register_settings' ) );
		add_action( 'admin_enqueue_scripts', array( $this, 'action_enqueue_scripts' ) );
		add_action( 'wp_enqueue_scripts', array( $this, 'action_enqueue_scripts' ) );
	}

	public function create_admin_menu() {
		$this->_screens['main'] = add_options_page(
			__( 'Strattic Settings', 'wp-spotim' ), 
			__( 'Strattic Settings', 'wp-spotim' ), 
			'manage_options', 
			$this->slug, 
			array( $this, 'admin_page_callback' )
		);
		// Just make sure we are create instance.
		add_action( 'load-' . $this->_screens['main'], array( &$this, 'load_cb' ) );
	}

	public function action_enqueue_scripts() {
		if ( ! current_user_can( 'manage_options' ) ) {
			return;
		}

		wp_enqueue_style( 'strattic-admin', $this->plugin->locations['url'] . 'ui/css/admin.css' );
		wp_enqueue_script( 'strattic-admin', $this->plugin->locations['url'] . 'ui/js/admin.js', array( 'jquery', 'underscore' ), false, true );

		$settings = array(
			'siteURL' => site_url(),
		);

		wp_localize_script( 'strattic-admin', 'Strattic', array(
			'AJAXURL' => admin_url( 'admin-ajax.php' ),
			'StratticDeploy' => $settings,
		) );
	}

	public function load_cb() {}

	public function get_options() {
		// Allow other plugins to get spotim's options.
		if ( isset( $this->options ) && is_array( $this->options ) && ! empty( $this->options ) )
			return $this->options;
		
		return apply_filters( 'spotim_options', get_option( $this->slug, array() ) );
	}

	public function validate_options( $input ) {
		$options = $this->options; // CTX,L1504
		
		// @todo some data validation/sanitization should go here
		$output = apply_filters( 'strattic_validate_options', array_map('trim', $input), $options );
		// merge with current settings
		$output = array_merge( $options, $output );
		
		return $output;
	}

	public function register_settings() {
		$this->options = $this->get_options();
		// If no options exist, create them.
		if ( ! get_option( $this->slug ) ) {
			update_option( $this->slug, apply_filters( 'spotim_default_options', array(
				'use_auth' => false,
				'new_domain' => false,
				'custom_bucketname' => false,
			) ) );
		}
		register_setting( 'strattic-options', $this->slug, array( $this, 'validate_options' ) );
		add_settings_section(
			'general_settings_section',			// ID used to identify this section and with which to register options
			__( 'Deployment Options', 'wp-spotim' ),	// Title to be displayed on the administration page
			array( 'Strattic_Settings_Fields', 'general_settings_section_header' ),	// Callback used to render the description of the section
			$this->slug		// Page on which to add this section of options
		);
		/*add_settings_field(
			'enable_comments_replacement',
			__( 'Enable Spot.IM comments replacement?', 'wp-spotim' ),
			array( 'Strattic_Settings_Fields', 'yesno_field' ),
			$this->slug,
			'general_settings_section',
			array(
				'id'      => 'enable_comments_replacement',
				'page'    => $this->slug,
			)
		);*/

		add_settings_field(
			'new_domain',
			__( 'New Domain URL', 'wp-spotim' ),
			array( 'Strattic_Settings_Fields', 'text_field' ),
			$this->slug,
			'general_settings_section',
			array(
				'id'      => 'new_domain',
				'page'    => $this->slug,
				'desc'	  => 'Default is (empty) a random S3 bucket that will be created automatically.'
			)
		);
		
		add_settings_field(
			'use_auth',
			__( 'Use HTTP Auth?', 'wp-spotim' ),
			array( 'Strattic_Settings_Fields', 'yesno_field' ),
			$this->slug,
			'general_settings_section',
			array(
				'id'      => 'use_auth',
				'page'    => $this->slug,
			)
		);

		add_settings_field(
			'auth_username',
			__( 'Auth Username', 'wp-spotim' ),
			array( 'Strattic_Settings_Fields', 'text_field' ),
			$this->slug,
			'general_settings_section',
			array(
				'id'      => 'auth_username',
				'page'    => $this->slug
			)
		);
		add_settings_field(
			'auth_pass',
			__( 'Auth Password', 'wp-spotim' ),
			array( 'Strattic_Settings_Fields', 'text_field' ),
			$this->slug,
			'general_settings_section',
			array(
				'id'      => 'auth_pass',
				'page'    => $this->slug
			)
		);

		add_settings_field(
			'custom_bucketname',
			__( 'Custom S3 Bucket Name', 'wp-spotim' ),
			array( 'Strattic_Settings_Fields', 'text_field' ),
			$this->slug,
			'general_settings_section',
			array(
				'id'      => 'custom_bucketname',
				'page'    => $this->slug,
				'desc'	  => '(optional) Use for custom S3 bucket name.'
			)
		);
	}

	public function admin_page_callback() {
		?>
		<div class="wrap">
			<div id="icon-themes" class="icon32"></div>
			<h2 class="spotim-page-title"><?php _e( 'Strattic Settings', 'wp-spotim' ); ?></h2>

			<form method="post" action="options.php">
				<?php
				settings_fields( 'strattic-options' );
				do_settings_sections( $this->slug );
				submit_button();
				?>
			</form>
		</div>
		<?php
	}

	public function get_option( $key = '', $default_value = false ) {
		$settings = $this->get_options();
		return ! empty( $settings[ $key ] ) ? $settings[ $key ] : $default_value;
	}


	function custom_toolbar() {
		global $wp_admin_bar;

		$args = array(
			'id'     => 'strattic_deploy',
			'title'  => __( 'Deploy to Strattic', 'text_domain' ),
			'group'  => false,
			'href' 	=> '#',
			'meta'   => array(
					'class'    => 'strattic-deploy',
				),
		);
		$wp_admin_bar->add_menu( $args );
	}

	function do_request() {
		$options = $this->get_options();

		// front and back-end alike
		$deploy_api_url = 'https://x58nchiay3.execute-api.us-east-1.amazonaws.com/dev/scrape';

		$request_data = array(
			'url' => home_url(),
		);

		if (!empty($options['new_domain'])) {
			$request_data['newDomain'] = esc_url($options['new_domain']);
		}

		// auth settings if any
		if ( isset($options['use_auth']) && $options['use_auth'] && !empty($options['auth_username']) && !empty($options['auth_pass']) ) {
			$request_data['auth'] = array(
				'username' => $options['auth_username'],
				'password' => $options['auth_pass'],
			);
		}

		// bucket name
		if ( !empty($options['custom_bucketname']) ) {
			$request_data['bucketName'] = $options['custom_bucketname'];
		}

		$response = wp_remote_post( $deploy_api_url, array(
			'method' => 'POST',
			'timeout' => 120, // long since it's lambda
			'headers' => array(
				'Content-Type' => 'application/json',
			),
			'body' => json_encode($request_data),
		) );

		if ( is_wp_error( $response ) ) {
			wp_send_json_error( 'something wrong with response ' . $response->get_error_message() );
		} else {
			wp_send_json_success( wp_remote_retrieve_body($response) );
		}

		exit;
	}

	function footer_print_js() {
		?>

	<script type="text/template" id="strattic-modal-template">
		<div class="strattic-modal ">
			<a href="#" class="strattic-modal-close button">&times;</a>

			<h2 class="strattic-modal-title">Deploying with Strattic</h2>

			<div class="strattic-modal-content">
				<p>Strattic has started deployment for <a href="<%= siteURL %>" target="_blank"><%= siteURL %></a>. You may <a href="#" style="color: #a00;">halt deployment</a>.</p>

				<div class="strattic-progress-bar">
					<div class="strattic-progress-meter" data-pos="0">0%</div>
				</div>
				<p class="strattic-status"><strong>Step:</strong> <span class="strattic-current-status">Initializing</span><span class="strattic-dots saving"><span>.</span><span>.</span><span>.</span></span></p>
			</div>
		</div>
	</script>
		<?php
	}
}