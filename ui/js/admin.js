jQuery(function($) {
	var Strattic = window.Strattic;


	var Modal = {
		setupModal: function () {
			var template = $('#strattic-modal-template').html(),
				compile = _.template(template);

			$('body').append(compile(Strattic.StratticDeploy));

			$('.strattic-modal-close').click(function() {
				$(this).closest('.strattic-modal').remove();
			});

			var $modal = $('.strattic-modal');

			// setup progress bar
			this.$progressBar = $('.strattic-progress-meter', $modal);
			this.$status = $('.strattic-status .strattic-current-status', $modal);
			this.$threeDots = $('.strattic-dots', $modal);
		},
		startProgressBar: function (cb) {
			var self = this;
			var $progressBar = this.$progressBar;

			// randomize timeout between 1 and 3 seconds
			var calculatedTimeout = this._randomRange(800, 1000) * this._randomRange(1, 3);
			var curPos = parseInt($progressBar.data('pos'));

			var newPos = curPos + this._randomRange(5, 15);

			this.updateProgressBar(newPos >= 100 ? 100 : newPos);

			if (newPos >= 100) {
				return cb();
			}

			// re-schedule
			setTimeout(function() {
				self.startProgressBar(cb);
			}, calculatedTimeout);
		},
		_randomRange: function (min, max) {
			return Math.floor(Math.random()*(max-min+1)+min);
		},
		updateCurrentStatus: function (statusText) {
			this.$status.text(statusText);
		},
		updateProgressBar: function(newPos) {
			var pct = newPos + '%';

			this.$progressBar.data('pos', newPos);
			this.$progressBar.text(pct);
			this.$progressBar.css('width', pct);
		},
		endProgressBar: function() {
			this.$threeDots.remove();
			this.$progressBar.addClass('strattic-done');
		}
	};


	$('#wpadminbar .strattic-deploy').click(function(e) {
		e.preventDefault();

		// set loading status
		var $menu = $(this).find('.ab-item'),
			originalText = $menu.text();

		Modal.setupModal();
		
		$menu.text('Notifying Strattic...');
		Modal.updateCurrentStatus('Contacting Strattic\'s deployment service');

		var data = {
			action: 'strattic-do-deploy'
		};

		// do request
		$.post(Strattic.AJAXURL, data, function(data) {
			var parsed = JSON.parse(data.data);

			$menu.text(originalText);

			if (data.success && parsed.success && parsed.success !== undefined) {

				// Update text in small popup
				Modal.startProgressBar(function () {
					// callback when progress bar is done
					Modal.updateCurrentStatus('Finished deployment');
					Modal.endProgressBar();
				});
				Modal.updateCurrentStatus('Deploying');

			} else {
				var parsedResponse = JSON.stringify(data);
				window.alert('Deployment request failed! Please notify the Strattic team with this error code: ' + parsedResponse);
			}
		});
	});
});